<?php defined('BASEPATH') OR exit('No direct script acces allowed');
class  Pemesanan extends CI_Controller {

	public function __construct()
		{
			parent::__construct();
			//load model terkait (manggil pertama kali scrip di jalanin)
			$this->load->model("Pemesanan_models");
			$this->load->model("Karyawan_models");
			$this->load->model("Menu_models");
			
		}

	public function index()
		{
			$this->listpemesanan();


		}
	public function listpemesanan()
		{
			$data['data_Karyawan'] 	= $this->Karyawan_models->tampilDataKaryawan();
			$data['data_menu'] 		= $this->Menu_models->tampilDataMenu();
			$data['data_pemesanan'] = $this->Pemesanan_models->tampilDataPemesanan2();

			$this->load->view('listpemesanan', $data);
		}
	public function detailpemesanan($id_pemesanan)
		{
			$data['data_pemesanan'] =$this->Pemesanan_models->detailpemesanan($id_pemesanan);
			$this->load->view('detailpemesanan', $data);
		}
	public function inputpemesanan()
		{
			$data['data_karyawan'] 	= $this->Karyawan_models->tampilDataKaryawan();
			$data['data_menu'] 		= $this->Menu_models->tampilDataMenu();
			$data['data_pemesanan'] = $this->Pemesanan_models->tampilDataPemesanan2();
				
				if (!empty($_REQUEST)){
				$m_pemesanan = $this->Pemesanan_models;
				$m_pemesanan->save('');
				redirect("Pemesanan/index", "refresh");
				}


			$this->load->view('inputpemesanan',$data);
		}
	
	public function delete($id_pemesanan)
	{
		$m_pemesanan = $this->Pemesanan_models;
		$m_pemesanan->delete($id_pemesanan);	
		redirect("Pemesanan/index", "refresh");	
	}	
	
}