<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_models extends CI_Model
{ 
	//panggil nama tabel
	private $_table = "master_menu";

	public function tampilDataMenu()
		{
			//seperti : select * from <nama_table>
			return $this->db->get($this->_table)->result();
		}

	public function tampilDataMenu2()
		{
			//KETIKA MAKE QUERY
			$query = $this->db->query("SELECT * FROM master_menu WHERE flag = 1");
			return $query->result();
		}

	public function cariHargaMenu($kode_menu)
		{
			$query_harga = $this->db->query("select harga from master_menu WHERE kode_menu = '".$kode_menu."'");
			return $query_harga->result_array();
		}


	public function tampilDataMenu3()
		{
			//MAKE QUERY BUILDER
			$this->db->select('*');
			$this->db->order_by('kode_menu', 'ASC');
			$result = $this->db->get($this->_table);
			return $result->result();
		}
	public function save()
		{
			
			$data['kode_menu'] 			=$this->input->post('kode_menu');
			$data['nama_menu']		 	=$this->input->post('nama_menu');
			$data['harga'] 				=$this->input->post('harga');
			$data['keterangan'] 		=$this->input->post('keterangan');
			$data['flag'] 			=1;
			$this->db->insert($this->_table, $data);
		}


	public function detailmenu($kode_menu)
	{
		$this->db->select('*');
		$this->db->where('kode_menu', $kode_menu);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}

	public function update($kode_menu)
	{
		$data['nama_menu']			= $this->input->post('nama_menu');
		$data['harga']			    = $this->input->post('harga');
		$data['keterangan']			= $this->input->post('keterangan');
		$data['flag']				= 1;
		
		$this->db->where('kode_menu', $kode_menu);
		$this->db->update($this->_table, $data);
	}
	public function delete($kode_menu)
	
		{
			$this->db->where('kode_menu',$kode_menu);
			$this->db->delete($this->_table);
		}

} 