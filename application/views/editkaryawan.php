<body>
   <?php
  foreach ($detail_karyawan as $data) {
    $nik = $data->nik;
    $nama= $data->nama;
    $alamat = $data->alamat;
    $telp = $data->telp;
    $tempat_lahir = $data->tempat_lahir;
    $tanggal_lahir = $data->tanggal_lahir;

  }
  //pisah tanggal bulan tahun
  $thn_pisah = substr($tanggal_lahir, 0, 4);
  $bln_pisah = substr($tanggal_lahir, 5, 2);
  $tgl_pisah = substr($tanggal_lahir, 8, 2);
  ?>

  <form action="<?=base_url()?>Karyawan/editkaryawan/<?= $nik; ?>" method="POST">
<table width="46%" border="0" cellspacing="0" cellpadding="5" bgcolor="green">
 
  <tr>
    <td width="43%">Nik</td>
    <td width="5%">:</td>
    <td width="52%">
      <input value="<?=$nik;?>" type="text" name="nik" id="nik" readonly/>
    </td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>:</td>
    <td>
      <input value="<?=$nama;?>"  type="text" name="nama" id="nama" />
     </td>
</td>
  </tr>
 
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td>
      <textarea name="alamat" id="alamat" cols="45" rows="5"><?=$alamat; ?></textarea>
    </td>
  </tr>

  <tr>
    <td>Telpon</td>
    <td>:</td>
    <td>
      <input value="<?=$telp; ?>" type="text" name="telpon" id="telpon" />
    </td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td>
      <input value="<?=$tempat_lahir; ?>" type="text" name="tempat_lahir" id="tempat_lahir" />
     </td>
</td>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td>
      <select name="tgl" id="tgl">
      <?php 
			for($tgl=1;$tgl<=31;$tgl++){
          $select_tgl = ($tgl == $tgl_pisah) ? 'selected' : '';					
	  ?>
	  <option value="<?= $tgl; ?>"<?= $select_tgl; ?>><?= $tgl; ?></option>
	  <?php 
			 }
	  ?>
      </select>
      
      
      <select name="bulan" id="bulan">
      <?php
		$n_bulan = array ('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus',							                           'Oktober','September','November','Desember');
							
				for($bulan=1;$bulan<=12;$bulan++){
        	 $select_bulan = ($bulan == $bln_pisah) ? 'selected' : '';
           ?>
           <option value="<?=$bulan;?>"<?=$select_bulan; ?>>
             <?=$n_bulan[$bulan];?>
           </option> 
					
	    <?php
			 }
	   ?>

    </select>
      
     
     <select name="tahun" id="tahun">
      <?php
				for($tahun=date('Y')-60;$tahun<=date('Y')-15;$tahun++){
        $select_tahun = ($tahun == $thn_pisah) ? 'selected' : '';	
					
		?>
				<option value="<?=$tahun;?>" <?=$select_tahun; ?>><?=$tahun;?> </option>
	 <?php
				}
			?>  
      
      </select>
      
    </td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="Submit" name="Submit" id="Submit" value="Simpan" />
      <input type="reset" name="reset" id="reset" value="Reset" />
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="Submit" name="Submit" id="Submit" value="kembali ke Menu Sebelumnya" />
      <a href="<?=base_url();?>Karyawan/listkaryawan"><font color="white">kembali ke Menu Sebelumnya</font></a>
  </tr>
  
</table>
</form>

</body>
